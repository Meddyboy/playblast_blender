# Blender Render Tool
Blender addon mainly focused on rendering the scene animation without modifying the current render settings.
<br>Written by Meddy Boukheddouma and Cosmin Planchon for the git project management exercise from Artline promotion 2022. 
<br>Please report bugs via [Issues > Boards](../../boards) in the left sidebar. 
 
## Installation
Download from https://gitlab.com/Meddyboy/playblast_blender/-/archive/main/playblast_blender-main.zip
or https://gitlab.com/sharedaccount/exo_git/-/archive/v1/exo_git-v1.zip
then install the addon via the Blender addon panel in the preferences window and enable it.
for more infos on installing a Blender addon see : https://youtu.be/vYh1qh9y1MI
 
You can also test the latest development version by downloading this repository
```
git clone https://gitlab.com/Meddyboy/playblast_blender.git
```
then copy the exo_git folder to the Blender addons directory to be able to see and activate the addon from the Blender Addon tab in the Preference window.
 
 
## Documentation
All the settings of the addon are set only for the Playblast rendering without altering the scene settings.
Useful for artists who do not wish to change their render configuration just to render a preview.
### Blender Render Tool main panel in Properties > Render
![BRT Main Panel in Properties window](cameramanager/ressources/brt_main_panel.jpg "Header button")
 
#### Presets :
Preset list to store and retrieve settings for the Blender Render Tool. 
 
#### Quick playblast :
Main button used to render the animation playblast.
 
#### Start & End frame : 
Frame range to use when rendering the animation playblast.
 
#### Render percentage : 
Render the playblast faster by reducing the percentage of the current render size settings.
 
#### Render engine :
Render engine to be used for rendering the playblast.
 
#### Options :
 
##### Include Audio from sequencer :
If audio tracks are present in the video sequencer they will be added to the playblast with this option.
 
##### Use simplify :
Enables simplify and set the subdivision level to 0 during the playblast rendering time.
 
##### Only selected :
If this option is enabled only the selected objects will be rendered during the playblast.
 
##### Burn Stamps :
Add a stamp with the frame number on the playblast. Additional parameters are available in the native Blender stamps panel.  
 
##### Copy to :
Copies the rendered preview.mov to the chosen folder after rendering the playblast. 
 
### Additional Playblast button in the 3D view header menu
 
![Brt button in the 3D view header menu](cameramanager/ressources/brt_top_menu.jpg "BRT Header")
<br>
<em>A Playblast button is also available from the 3D view header menu</em>
 
 

